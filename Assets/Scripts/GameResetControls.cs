﻿using System;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(LongPressGesture))]
public class GameResetControls : MonoBehaviour {

    private LongPressGesture m_longPressGesture;

    private void Awake()
    {
        m_longPressGesture = GetComponent<LongPressGesture>();
    }

    void Start()
    {
        m_longPressGesture.LongPressed += OnLongPress;
    }

    private void OnLongPress(object sender, EventArgs e)
    {
        Debug.Log("Reset current animal");
        string currentScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentScene);
    }
}
