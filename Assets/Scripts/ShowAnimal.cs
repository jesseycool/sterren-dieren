﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ShowAnimal : MonoBehaviour {

    [SerializeField]
    private StarGroup m_starGroup;
    private SpriteRenderer m_sprite;

    private void Awake()
    {
        m_sprite = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        m_starGroup.OnPictureDoneEvent += OnPictureDone;
    }

    private void OnPictureDone()
    {
        m_sprite.enabled = true;
    }
}
