﻿using System;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(PressGesture))]
public class GameNextControls : MonoBehaviour
{

    private PressGesture m_pressGesture;
    [SerializeField] private ShowAnimalSound m_sound;
    private SpriteRenderer m_sprite;
    private CircleCollider2D m_collider;
    private AsyncOperation async = null;
    private bool m_pressed = false;

    private void Awake()
    {
        m_sprite = GetComponent<SpriteRenderer>();
        m_collider = GetComponent<CircleCollider2D>();
        m_pressGesture = GetComponent<PressGesture>();
    }

    private void Start()
    {
        m_sound.OnSoundDoneEvent += Init;
    }

    void Init()
    {
        m_sprite.enabled = true;
        m_collider.enabled = true;
        m_pressGesture.StateChanged += OnPress;
    }

    private void OnPress(object sender, GestureStateChangeEventArgs e)
    {
        if(!m_pressed)
        {
            m_pressed = true;

            Debug.Log("Next animal");
            PlayerPrefs.SetInt("lastScene", SceneManager.GetActiveScene().buildIndex);
            //SceneManager.LoadScene(0);
            StartCoroutine(LoadLevel());
        }
    }
    
    private IEnumerator LoadLevel()
    {
        async = SceneManager.LoadSceneAsync(0);
        yield return async;
    }
    void OnGUI()
    {
        if (async != null)
        {
            Debug.Log(async.progress * 100);
        }
    }
}
