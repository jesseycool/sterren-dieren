﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

[RequireComponent(typeof(Light))]
[RequireComponent(typeof(PressGesture))]
[RequireComponent(typeof(ReleaseGesture))]
public class StarConnection : MonoBehaviour
{
    public const int LINE_SIZE = 100;
    public const int BEZIER_RESOLUTION = 60;

    [SerializeField] private ColorSettings m_colors;
    private Light m_light;

    private GlowingStar m_glowfx;
    private BezierCurve m_bezierA, m_bezierB;
    private LineRenderer m_lineA, m_lineB;
    [SerializeField] private float m_progressA, m_progressB;
    private PressGesture m_pressGesture;
    private ReleaseGesture m_releaseGesture;
    private bool m_isPressed = false;
    private bool m_lineADone, m_lineBDone;
    private Vector3 m_lineAEnd, m_lineBEnd;

    private StarGroup m_group;

    public Color BezierA
    {
        get
        {
            return m_bezierA.drawColor;
        }
    }

    public Color BezierB
    {
        get
        {
            return m_bezierB.drawColor;
        }
    }

    public float ProgressA
    {
        get
        {
            return m_progressA;
        }

        set
        {
            m_progressA = value;
        }
    }

    public float ProgressB
    {
        get
        {
            return m_progressB;
        }

        set
        {
            m_progressB = value;
        }
    }

    public bool LineADone
    {
        get
        {
            return m_lineADone;
        }

        set
        {
            m_lineADone = value;
        }
    }

    public bool LineBDone
    {
        get
        {
            return m_lineBDone;
        }

        set
        {
            m_lineBDone = value;
        }
    }

    public Vector3 LineAEnd
    {
        get
        {
            return m_lineAEnd;
        }
    }

    public Vector3 LineBEnd
    {
        get
        {
            return m_lineBEnd;
        }

    }

    public GlowingStar Glowfx
    {
        get
        {
            return m_glowfx;
        }

        set
        {
            m_glowfx = value;
        }
    }

    private void Awake()
    {
        m_light = GetComponent<Light>();
        m_glowfx = GetComponent<GlowingStar>();
        m_group = GetComponentInParent<StarGroup>();
        m_pressGesture = GetComponent<PressGesture>();
        m_releaseGesture = GetComponent<ReleaseGesture>();
    }

    void Start()
    {
        m_bezierA = transform.GetChild(0).GetComponent<BezierCurve>();
        m_bezierB = transform.GetChild(1).GetComponent<BezierCurve>();

        m_pressGesture.StateChanged += OnPress;
        m_releaseGesture.StateChanged += OnRelease;

        m_lineA = GetNewLine();
        m_lineB = GetNewLine();

        m_group.OnPictureDoneEvent += OnPictureDone;

        m_progressA = m_progressB = 1; //wake it up
    }

    private LineRenderer GetNewLine()
    {
        GameObject go = Resources.Load<GameObject>("Line");
        GameObject goLine = Instantiate(go, this.transform);
        LineRenderer line = goLine.GetComponent<LineRenderer>();
        line.enabled = true;
        return line;
    }

    private void Update()
    {
        UpdateLazer(m_bezierA, m_lineA, m_progressA, ref m_lineAEnd);
        UpdateLazer(m_bezierB, m_lineB, m_progressB, ref m_lineBEnd);

        if (m_lineADone) { }
        else
        {
            if (!m_isPressed)
            {
                m_progressA -= 0.5f;
                if (m_progressA < 0)
                {
                    m_progressA = 0;
                }
            }
        }

        if (m_lineBDone) { }
        else
        {
            if (!m_isPressed)
            {
                m_progressB -= 0.5f;
                if (m_progressB < 0)
                {
                    m_progressB = 0;
                }
            }
        }

        if ((m_lineADone || m_lineBDone) && m_isPressed)
        {
            m_light.color = m_colors.SuccesColor;
        }

        transform.GetChild(2).SetPositionAndRotation(m_lineAEnd, Quaternion.identity);
        transform.GetChild(3).SetPositionAndRotation(m_lineBEnd, Quaternion.identity);
    }

    private void UpdateLazer(BezierCurve bezier, LineRenderer lazer, float progress, ref Vector3 lineEnd)
    {
        int limit = bezier.resolution + 1;
        float _res = bezier.resolution;

        Vector3[] positions = new Vector3[LINE_SIZE];
        float lastPointPresent = 0f;
        for (int i = 0; i < (int)progress; i++)
        {
            Vector3 currentPoint = bezier.GetPointAt((i / _res) * bezier.pointCount);
            positions[i] = currentPoint;
            lineEnd = currentPoint;
            lastPointPresent = (i / _res) * bezier.pointCount;
        }

        for (int j = (int)progress; j < LINE_SIZE; j++)
        {
            Vector3 last = bezier.GetPointAt(lastPointPresent);
            positions[j] = last;
        }

        lazer.SetPositions(positions);
    }

    private void OnPress(object sender, GestureStateChangeEventArgs e)
    {
        m_light.color = m_colors.HighLightColor;
        m_isPressed = true;
        m_group.AddSelectedStar(this);
    }

    private void OnRelease(object sender, GestureStateChangeEventArgs e)
    {
        m_light.color = m_colors.DefaultLightColor;
        m_isPressed = false;
        m_group.RemoveSelectedStar(this);
    }

    private void OnPictureDone()
    {
        m_lineA.enabled = false;
        m_lineB.enabled = false;
        m_glowfx.SetRunning(false);
    }
}
