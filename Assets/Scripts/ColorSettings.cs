﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "New Color Settings", menuName = "Project Chaos/Star Settings", order = 1)]
public class ColorSettings : ScriptableObject
{

    [SerializeField] private Color m_defaultLightColor;
    [SerializeField] private Color m_highLightColor;
    [SerializeField] private Color m_succesColor;

    public Color DefaultLightColor
    {
        get
        {
            return m_defaultLightColor;
        }
    }

    public Color HighLightColor
    {
        get
        {
            return m_highLightColor;
        }
    }

    public Color SuccesColor
    {
        get
        {
            return m_succesColor;
        }

    }
}