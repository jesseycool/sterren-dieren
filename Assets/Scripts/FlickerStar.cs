﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerStar : MonoBehaviour
{

    private float m_flickerSpeed = 0.07f;
    private int m_randomizer = 0;
    private Light m_light;

    private void Awake()
    {
        m_light = GetComponent<Light>();
    }

    private void Start()
    {
        StartCoroutine(StartLight());
    }

    IEnumerator StartLight()
    {
        while (true)
        {
            if (m_randomizer == 0)
            {
                m_light.enabled = true;
            }
            else m_light.enabled = false;
            m_randomizer = (int)Random.Range(0, 1.1f);
            yield return new WaitForSeconds(m_flickerSpeed);
        }
    }
}
