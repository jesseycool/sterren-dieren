﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ShowAnimalSound : MonoBehaviour
{
    public delegate void OnSoundDoneEventDelegate();
    public event OnSoundDoneEventDelegate OnSoundDoneEvent;

    [SerializeField]
    private StarGroup m_starGroup;
    private AudioSource m_audiosource;
    private bool m_checkPlaying = false;

    private void Awake()
    {
        m_audiosource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        m_starGroup.OnPictureDoneEvent += OnPictureDone;
    }

    private void OnPictureDone()
    {
        m_audiosource.Play();
        m_checkPlaying = true;
    }

    private void Update()
    {
        if(m_checkPlaying && !m_audiosource.isPlaying)
        {
            if(OnSoundDoneEvent != null) { OnSoundDoneEvent(); }
        }
    }
}
