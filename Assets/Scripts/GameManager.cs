﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	
	void Update () {
        foreach (Touch t in Input.touches)
        {
            if (t.phase == TouchPhase.Began)
            {
                Vector3 point = new Vector3(t.position.x, t.position.y, 0);
                Ray ray = Camera.main.ViewportPointToRay(point);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    hit.collider.SendMessage("OnMouseDown", null, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
}
