﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{

    [SerializeField]
    private Color m_highLightColor;
    //private Color m_defaultColor;
    private Light m_light;
    private LineRenderer m_line;

    [SerializeField]
    private Transform m_testTransform;

    private void Start()
    {
        m_light = GetComponent<Light>();
        //m_defaultColor = m_light.color;

        GameObject go = Resources.Load<GameObject>("Line");
        GameObject goLine = Instantiate(go, this.transform);
        m_line = goLine.GetComponent<LineRenderer>();
        m_line.enabled = false;
    }

    private void OnMouseDown()
    {
        m_light.color = m_highLightColor;

        if (m_testTransform)
        {
            UpdateLazer(m_testTransform.position, m_line);
        }
    }

    private void UpdateLazer(Vector3 to, LineRenderer lazer)
    {
        m_line.enabled = true;
        lazer.SetPosition(0, this.transform.position);
        lazer.SetPosition(1, to);
    }
}
