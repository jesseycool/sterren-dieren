﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimalRandomizer : MonoBehaviour {

    [SerializeField] private int m_amountOfAnimals = -1;

    private void Start()
    {
        int ignore = PlayerPrefs.GetInt("lastScene");
        int newScene = Random.Range(1, m_amountOfAnimals + 1);

        if(newScene == ignore)
        {
            SceneManager.LoadScene(0); //do it agian
        }
        else
        {
            SceneManager.LoadScene(newScene);
        }
    }
}
