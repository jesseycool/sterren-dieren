﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(AudioSource))]
public class StarGroup : MonoBehaviour
{
    public delegate void OnPictureDoneEventDelegate();
    public event OnPictureDoneEventDelegate OnPictureDoneEvent;

    public List<StarConnection> m_stars;
    public List<StarConnection> m_selectedStars;
    [SerializeField]
    private float m_distance = -1f;
    private float m_grow = 0.5f;
    private AudioSource m_beep;

    private void Awake()
    {
        m_beep = GetComponent<AudioSource>();
    }

    private void Start()
    {
        m_selectedStars = new List<StarConnection>();
        m_stars = new List<StarConnection>();
        for (int i = 0; i < transform.childCount; i++)
        {
            StarConnection sc = transform.GetChild(i).GetComponent<StarConnection>();
            m_stars.Add(sc);
        }
    }

    private void Update()
    {
        if (m_selectedStars.Count > 1)
        {
            Color colorCode_0A = m_selectedStars[0].BezierA;
            Color colorCode_0B = m_selectedStars[0].BezierB;
            Color colorCode_1A = m_selectedStars[1].BezierA;
            Color colorCode_1B = m_selectedStars[1].BezierB;

            if (CompareColor(colorCode_0A, colorCode_1A))
            {
                UpdateBeepPitch(m_selectedStars[0].transform.position, m_selectedStars[1].transform.position);

                m_selectedStars[0] = KaasA(m_selectedStars[0]);
                m_selectedStars[1] = KaasA(m_selectedStars[1]);
                float distance = Vector3.Distance(m_selectedStars[0].LineAEnd, m_selectedStars[1].LineAEnd);
                distance = (float)Math.Round((Decimal)distance, 2, MidpointRounding.AwayFromZero);
                m_distance = distance;
                if (distance <= 0.1f)
                {
                    m_selectedStars[0].LineADone = true;
                    m_selectedStars[1].LineADone = true;
                    m_beep.mute = true;
                    CheckIfPictureDone();
                }
            }

            if (CompareColor(colorCode_0B, colorCode_1B))
            {
                UpdateBeepPitch(m_selectedStars[0].transform.position, m_selectedStars[1].transform.position);
                m_selectedStars[0] = KaasB(m_selectedStars[0]);
                m_selectedStars[1] = KaasB(m_selectedStars[1]);
                float distance = Vector3.Distance(m_selectedStars[0].LineBEnd, m_selectedStars[1].LineBEnd);
                distance = (float)Math.Round((Decimal)distance, 2, MidpointRounding.AwayFromZero);
                m_distance = distance;
                if (distance <= 0.1f)
                {
                    m_selectedStars[0].LineBDone = true;
                    m_selectedStars[1].LineBDone = true;
                    m_beep.mute = true;
                    CheckIfPictureDone();
                }
            }

            if (CompareColor(colorCode_0A, colorCode_1B))
            {
                UpdateBeepPitch(m_selectedStars[0].transform.position, m_selectedStars[1].transform.position);
                m_selectedStars[0] = KaasA(m_selectedStars[0]);
                m_selectedStars[1] = KaasB(m_selectedStars[1]);
                float distance = Vector3.Distance(m_selectedStars[0].LineAEnd, m_selectedStars[1].LineBEnd);
                distance = (float)Math.Round((Decimal)distance, 2, MidpointRounding.AwayFromZero);
                m_distance = distance;
                if (distance <= 0.1f)
                {
                    m_selectedStars[0].LineADone = true;
                    m_selectedStars[1].LineBDone = true;
                    m_beep.mute = true;
                    CheckIfPictureDone();
                }
            }

            if (CompareColor(colorCode_0B, colorCode_1A))
            {
                UpdateBeepPitch(m_selectedStars[0].transform.position, m_selectedStars[1].transform.position);
                m_selectedStars[0] = KaasB(m_selectedStars[0]);
                m_selectedStars[1] = KaasA(m_selectedStars[1]);
                float distance = Vector3.Distance(m_selectedStars[0].LineBEnd, m_selectedStars[1].LineAEnd);
                distance = (float)Math.Round((Decimal)distance, 2, MidpointRounding.AwayFromZero);
                m_distance = distance;
                if (distance <= 0.1f)
                {
                    m_selectedStars[0].LineBDone = true;
                    m_selectedStars[1].LineADone = true;
                    m_beep.mute = true;
                    CheckIfPictureDone();
                }
            }
        }
    }
    
    private void UpdateBeepPitch(Vector3 start, Vector3 end)
    {
        if(m_beep.mute) { m_beep.mute = false; }

        float totalDis = Vector3.Distance(start, end);
        totalDis = (float)Math.Round((Decimal)totalDis, 2, MidpointRounding.AwayFromZero);

        float f = totalDis / m_distance;
        m_beep.pitch = Mathf.Clamp(f, 0.5f, 3);
    }

    private StarConnection KaasA(StarConnection star)
    {
        star.ProgressA += m_grow;
        if (star.ProgressA > StarConnection.BEZIER_RESOLUTION)
        {
            star.ProgressA = StarConnection.BEZIER_RESOLUTION;
        }
        return star;
    }

    private StarConnection KaasB(StarConnection star)
    {
        star.ProgressB += m_grow;
        if (star.ProgressB > StarConnection.BEZIER_RESOLUTION)
        {
            star.ProgressB = StarConnection.BEZIER_RESOLUTION;
        }
        return star;
    }

    public void AddSelectedStar(StarConnection star)
    {
        m_selectedStars.Add(star);
    }

    public void RemoveSelectedStar(StarConnection star)
    {
        m_selectedStars.Remove(star);
    }

    private void CheckIfPictureDone()
    {
        int count = 0;
        foreach(StarConnection sc in m_stars)
        {
            if(sc.LineADone && sc.LineBDone)
            {
                count++;
            }
        }
        if(count >= m_stars.Count)
        {
            if (OnPictureDoneEvent != null) OnPictureDoneEvent();
        }
    }

    private bool CompareColor(Color colour1, Color colour2)
    {
        bool r = Mathf.Abs(((colour1.r * 1000) - (colour2.r * 1000))) < 3f; //1.4f is het verschil
        bool g = Mathf.Abs(((colour1.g * 1000) - (colour2.g * 1000))) < 3f;
        bool b = Mathf.Abs(((colour1.b * 1000) - (colour2.b * 1000))) < 3f;
        return r && g && b;
    }
}
