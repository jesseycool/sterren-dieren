﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class GlowingLine : MonoBehaviour
{
    [SerializeField]
    private float m_flickerSpeed = 0.07f;
    private int m_randomizer = 0;
    private LineRenderer m_line;

    private void Start()
    {
        m_line = GetComponent<LineRenderer>();

        StartCoroutine(StartGlowing());
    }

    IEnumerator StartGlowing()
    {
        while (true)
        {
            if (m_randomizer == 0)
            {
                float r = Random.Range(0.08f, 0.1f);
                m_line.startWidth = r;
            }
            m_randomizer = (int)Random.Range(0, 1.1f);
            yield return new WaitForSeconds(m_flickerSpeed);
        }
    }
}
