﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class GlowingStar : MonoBehaviour
{
    [SerializeField]
    private float m_flickerSpeed = 0.07f;
    private int m_randomizer = 0;
    private Light m_light;
    [SerializeField]
    private bool m_isRunning = false;
    private float m_time = 0;

    private void Awake()
    {
        m_light = GetComponent<Light>();
    }
    
    public void SetRunning(bool b)
    {
        m_isRunning = b;
    }

    private void Update()
    {
        if (m_isRunning)
        {
            m_time += Time.deltaTime;
            if (m_time >= m_flickerSpeed)
            {
                m_time = 0f;
                if (m_randomizer == 0)
                {
                    float r = Random.Range(0.7f, 0.9f);
                    m_light.range = r;
                }
                m_randomizer = (int)Random.Range(0, 1.1f);
            }
        }
        else
        {
            if (m_light.range != 1)
            {
                m_light.range = 0.7f;
            }
        }
    }
}
