﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayAnimal : MonoBehaviour
{

    [SerializeField]
    private List<StarConnection> m_starSpawnOrder;
    private int m_index = 0;

    private void Start()
    {
        foreach (StarConnection sc in m_starSpawnOrder)
        {
            sc.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (m_index + 1 < m_starSpawnOrder.Count)
        {
            m_starSpawnOrder[m_index].gameObject.SetActive(true);
            m_starSpawnOrder[m_index].Glowfx.SetRunning(true);
            m_starSpawnOrder[m_index + 1].gameObject.SetActive(true);
            m_starSpawnOrder[m_index + 1].Glowfx.SetRunning(true);

            bool cheack = (m_starSpawnOrder[m_index].LineADone || m_starSpawnOrder[m_index].LineBDone)
                && (m_starSpawnOrder[m_index + 1].LineADone || m_starSpawnOrder[m_index + 1].LineBDone);

            if (cheack)
            {
                m_starSpawnOrder[m_index].Glowfx.SetRunning(false);
                m_index += 1;
            }
        }
    }
}
